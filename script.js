function initdark() {
    console.log("DEBUG :(")
    if (window.localStorage.getItem('darkmode') === null) {
		window.localStorage.setItem('darkmode', 'true');
		document.documentElement.classList.add('dark');
	} else {
		if (window.localStorage.getItem('darkmode') === 'true') {
			document.documentElement.classList.add('dark');
		}
	}
}

function darkToggle() {
	if (window.localStorage.getItem('darkmode') === 'true') {
		window.localStorage.setItem('darkmode', 'false');
		document.documentElement.classList.remove('dark');
		
	} else if (window.localStorage.getItem('darkmode') === 'false') {
		window.localStorage.setItem('darkmode', 'true');
		document.documentElement.classList.add('dark');
	}
}

window.onload = initdark()
  