/** @type {import('tailwindcss').Config} */
module.exports = {
  safelist: [
    'pb-5',
  ],
  darkMode: 'class',
  content: ["./**.{html,js}"],
  theme: {
    extend: {
      outlineOffset: {
        4: '-4px',
      }
    },
  },
  plugins: [],
}
